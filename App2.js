import React, {useState, useEffect, Component} from 'react';
import {Animated, Text, View, Easing, TouchableOpacity} from 'react-native';
import construct from "@babel/runtime/helpers/esm/construct";

const FadeInView = (props) => {
    const [fadeAnim] = useState(new Animated.Value(0))  // Initial value for opacity: 0

    React.useEffect(() => {
        Animated.timing(
            fadeAnim,
            {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear(),
            }
        ).start();
    }, []);

    return (
        <Animated.View                 // Special animatable View
            style={{
                ...props.style,
                opacity: fadeAnim,         // Bind opacity to animated value
            }}
        >
            {props.children}
        </Animated.View>
    );
}

// You can then use your `FadeInView` in place of a `View` in your components:
export default class App extends Component {

    constructor(props) {
        super(props);
        this.anim = new Animated.ValueXY();
    }

    onAction = () => {
        Animated.decay(this.anim, {
            velocity: {x: 50, y: 0},
            deceleration: 0.2
        }).start();
    };

    render(): React.ReactElement<any> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                {/*<FadeInView style={{width: 250, height: 50, backgroundColor: 'powderblue'}}>*/}
                {/*    <Text style={{fontSize: 28, textAlign: 'center', margin: 10}}>Fading in</Text>*/}
                {/*</FadeInView>*/}

                <TouchableOpacity onPress={this.onAction}>
                    <Animated.View style={this.anim.getLayout()}>
                        <Text>
                            Hello
                        </Text>
                    </Animated.View>
                </TouchableOpacity>
            </View>
        )
    }
}
