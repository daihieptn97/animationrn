/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Animated,
    Easing,
    Dimensions
} from 'react-native';

const screenWidth = Math.round(Dimensions.get('window').width);

export default class App extends Component {

    constructor() {
        super();
        this.anim = new Animated.Value(0);
    }

    componentDidMount(): void {
        this.runAnimate();
    }

    runAnimate = () => {
        this.anim.setValue(0);
        console.log(Easing.quad);
        // Animated.loop(
        //
        // ).start();

        Animated.sequence([

            Animated.timing(
                this.anim,
                {
                    toValue: 1,
                    duration: 1000,
                    easing: Easing.elastic(2)
                }
            ),
            // Animated.delay(1000),
            Animated.timing(
                this.anim,
                {
                    toValue: 0,
                    duration: 500,
                    easing: Easing.out(Easing.quad)
                }
            )
        ]).start();

    };

    render() {
        console.log(screenWidth / 2);
        const marginLeft = this.anim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, (screenWidth / 2) - 50],

        });


        return (
            <View style={[styles.container]}>
                <TouchableOpacity onPress={() => this.runAnimate()}>
                    <Animated.View style={[styles.button, {marginLeft}]}>
                        <Text style={styles.textButton}>Xin chao</Text>
                    </Animated.View>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        // alignItems: "center",
        backgroundColor: 'rgba(190,190,190,0.34)'
    },
    button: {
        justifyContent: "center",
        // backgroundColor: "grey",
        height: 50,
        width: 100,
        backgroundColor: '#b2dfdb',
        borderRadius: 8,
        borderWidth: 0,
        shadowOffset: {
            width: 1.5,
            height: 1.5,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2.5,
        elevation: 6,

    },
    textButton: {
        textAlign: "center",
        // color: "white"
    }
});
