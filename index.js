/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import App3 from './App3';
import Easing from './Easings';
// import App2 from './App2';
import Spring from "./Spring";
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Spring);
