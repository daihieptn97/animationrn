/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Animated,
    Image,
    Easing
} from 'react-native';


export default class App extends Component {
    constructor() {
        super();
        this.spinValue = new Animated.Value(0);
    }

    componentDidMount(): void {
        this.spin();
    }

    spin() {
        this.spinValue.setValue(0);
        Animated.timing(
            this.spinValue,
            {
                toValue: 1,
                duration: 4000,
                easing: Easing.linear,
                useNativeDriver: true
            }
        ).start(() => this.spin());
    }

    render() {
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });

        return (
            <View style={[styles.container]}>

                <Animated.Image
                    style={{
                        width: 200,
                        height: 200,
                        transform: [{rotate: spin}],
                        borderRadius: 100
                    }}
                    source={{uri: 'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/81161208_1512678368910055_1072934121040248832_o.jpg?_nc_cat=108&_nc_ohc=IOJPGLSHVv0AQnJ2iOymjuFzQjbXMMXZmotur9NFl8brQIlLxRvICpUYA&_nc_ht=scontent.fhan2-3.fna&oh=f1a7c9eacd5cde24f9dbf461f1e1e259&oe=5E94A057'}}
                />

                {/*<TouchableOpacity style={styles.button}>*/}
                {/*    <Text style={styles.textButton}>Xin chao</Text>*/}
                {/*</TouchableOpacity>*/}
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // backgroundColor: 'rgba(190,190,190,0.34)'
    },
    button: {
        justifyContent: "center",
        // backgroundColor: "grey",
        height: 60,
        width: 120,
        backgroundColor: '#fff',
        borderRadius: 8,
        borderWidth: 0,
        shadowOffset: {
            width: 1.5,
            height: 1.5,
        },
        shadowOpacity: 0.2,
        shadowRadius: 2.5,
        elevation: 6,

    },
    textButton: {
        textAlign: "center",
        // color: "white"
    }
});
